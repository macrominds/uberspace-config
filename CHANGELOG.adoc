= Change Log

All noteable changes to this project will be documented in this file.
This project adheres to http://semver.org/[Semantic Versioning].

== [0.3.1] – 2019-10-23

- Fix path to cwebp. It would only work in the home folder. Now it works in any directory.

== [0.3.0] – 2019-10-23

- Rename docroot folder from web to public
- fix missing home / local directory

== [0.2.0] – 2019-10-22

- Added libwebp support to be able to convert images using cwebp

== [0.1.0] – 2019-10-21

Initial working version



